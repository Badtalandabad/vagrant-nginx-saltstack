rbenv-deps:
  pkg.installed:
   - pkgs:
      - git-core
      - curl
      - zlib1g-dev
      - build-essential
      - libssl-dev
      - libreadline-dev
      - libyaml-dev
      - libsqlite3-dev
      - sqlite3
      - libxml2-dev
      - libxslt1-dev
      - libcurl4-openssl-dev
      - python-software-properties
      - libffi-dev

ruby-{{ pillar['ruby-version'] }}:
  rbenv.installed:
    - user: {{ pillar['user-name'] }}
    - default: True
    - require:
      - pkg: rbenv-deps

# rbenv env variables
/home/{{ pillar['user-name'] }}/.profile.d:
  file.directory:
    - makedirs: True
    - user: {{ pillar['user-name'] }}
    - group: {{ pillar['user-name'] }}
    
profile.d-rbenv-bin:
  file.managed:
    - user: {{ pillar['user-name'] }}
    - group: {{ pillar['user-name'] }}
    - name: /home/{{ pillar['user-name'] }}/.profile.d/rbenv-bin
      
/home/{{ pillar['user-name'] }}/.profile.d/rbenv-bin:  
  file.append:
    - text:
      - export PATH="$HOME/.rbenv/bin:$PATH"
      - eval "$(rbenv init -)"
    - require:      
      - file: profile.d-rbenv-bin
      
profile.d-rbenv-ruby-build-bin:
  file.managed:
    - user: {{ pillar['user-name'] }}
    - group: {{ pillar['user-name'] }}
    - name: /home/{{ pillar['user-name'] }}/.profile.d/rbenv-ruby-build-bin
      
/home/{{ pillar['user-name'] }}/.profile.d/rbenv-ruby-build-bin:
  file.append:
    - text:
      - export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
    - require:      
      - file: profile.d-rbenv-ruby-build-bin

# make rbenv paths available in the shell
/home/{{ pillar['user-name'] }}/.bash_profile :
  file.append:
    - text: |
        #
        . "$HOME/.profile.d/rbenv-bin"
        . "$HOME/.profile.d/rbenv-ruby-build-bin"
        #
    - require:
      - file: /home/{{ pillar['user-name'] }}/.profile.d/rbenv-bin
      - file: /home/{{ pillar['user-name'] }}/.profile.d/rbenv-ruby-build-bin
      
source /home/{{ pillar['user-name'] }}/.profile:
  cmd.run:
    - user: {{ pillar['user-name'] }}
      
# tell Rubygems not to install the documentation for each package locally
file-dot-gemrc:
  file.managed:
    - user: {{ pillar['user-name'] }}
    - group: {{ pillar['user-name'] }}
    - name: /home/{{ pillar['user-name'] }}/.gemrc

/home/{{ pillar['user-name'] }}/.gemrc:
  file.append:
    - text:
      - "gem: --no-ri --no-rdoc"
    - require:
      - file: file-dot-gemrc

rails-{{ pillar['rails-version'] }}:
  gem.installed:
    - name: rails
    - ruby: {{ pillar['ruby-version'] }}
    - user: {{ pillar['user-name'] }}
    - version: {{ pillar['rails-version'] }}
    - rdoc: false
    - ri: false    
    - require:
      - rbenv: ruby-{{ pillar['ruby-version'] }}
      - file: /home/{{ pillar['user-name'] }}/.gemrc

/var/www/{{ pillar['ruby-app'] }}/config/unicorn.rb:
  file.managed:
    - makedirs: True
    - source: salt://ror/unicorn.rb
    - template: jinja

shared_ror_dirs:
  file.directory:
    - makedirs: True
    - user: {{ pillar['user-name'] }}
    - group: {{ pillar['user-name'] }}
    - mode: 644
    - names:
      - /home/{{ pillar['user-name'] }}/{{ pillar['ruby-app'] }}/shared/pids
      - /home/{{ pillar['user-name'] }}/{{ pillar['ruby-app'] }}/shared/sockets
      - /home/{{ pillar['user-name'] }}/{{ pillar['ruby-app'] }}/shared/log

/etc/init.d/unicorn_ror:
  file.managed:
    - source: salt://ror/unicorn_initd
    - template: jinja
    - user: root
    - group: root
    - mode: 755
  cmd.wait:
    - name: update-rc.d unicorn_{{ pillar['ruby-app'] }} defaults

unicorn_{{ pillar['ruby-app'] }}:
  service.running:
    - enable: True
    - reload: True