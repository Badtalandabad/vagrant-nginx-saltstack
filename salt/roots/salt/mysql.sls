python-mysqldb:
  pkg.installed

mysql_server_installed:
  debconf.set:
    - name: mysql-server
    - data:
        'mysql-server/root_password': {'type': 'string', 'value': 'root'}
        'mysql-server/root_password_again': {'type': 'string', 'value': 'root'}
  pkg.installed:
    - pkgs:
      - mysql-server
      - mysql-client
      - libmysqlclient-dev

/etc/mysql/mysql.conf.d/mysqld.cnf:
  file.line:
    - match: "bind-address"
    - content: "bind-address = 0.0.0.0"
    - mode: replace
 
mysql_service_enabled:
  service.running:
    - name: mysql
    - enable: True
    - reload: True
 
root_from_all:
  mysql_user.present:
    - name: root
    - password: root
    - host: '%'
    - connection_user: root
    - connection_pass: 'root'
    - require:
      - mysql_service_enabled

grant_outside:
  mysql_grants.present:
    - grant: all privileges
    - database: "*.*"
    - user: root
    - host: "%"
    - grant_option: True
    - connection_user: root
    - connection_pass: 'root'

create_database:
  mysql_database.present:
    - connection_user: root
    - connection_pass: 'root'
    - connection_charset: utf8
    - name: 'banjo'