ifupdown:
  pkg.installed

python-pip:
  pkg.installed

banjo_hosts_file:
  host.present:
    - name: banjo.local
    - ip: 127.0.0.1
    
musicconf_hosts_file:
  host.present:
    - name: musicconf.local
    - ip: 127.0.0.1
    
add_public_ssh_key:
  file.append:
    - name: /home/{{ pillar['user-name'] }}/.ssh/authorized_keys
    - text: "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAlJPbnKmPkFkDyayWbZQDnksEd0DTQbvNHfEbLNXO0EUUKZ/2e1ojHiTA/urkO7DMtcDiPJXjxJGhVm6Ntg6/Azp777OZw3xAuoZBb3tXn2s1z8n7KQulbWuv56I6fcTYY1j874XKVyKH2kYoA0zWIsWOEsbdiDoopHsTDG/Z0CVOXwZS4ChzIuMGMgk5+7lTFlv6AaTddPx9CMzGHqYUS+AwWhB2HSt50Vgt3UGFbQwJYxNSZxt37O//GwhTidVH3PFqJk5FCGT8VWwnvqemr7e1Jz2RnXaPmV/26DsPNdAEjmbfGhuiGxkcVE21Cx+WJBj6qnhVhJIv/tJReCovQQ== rsa-key-20170607"

create_vimrc:
  file.managed:
    - name: /home/{{ pillar['user-name'] }}/.vimrc
    - replace: False
    - create: True
    
set_vim_number:
  file.append:
    - name: /home/{{ pillar['user-name'] }}/.vimrc
    - text: "set number"

/var/www/html/index.php:
  file.managed:
    - contents: "<?php phpinfo();"
    - makedirs: True
    #no mode and author since it is in the shared directory
    #- user: www-data
    #- group: www-data
    #- mode: 644
Europe/Samara:
  timezone.system:
    - utc: True