apache2:
  pkg:
    - removed

# banjo nginx log files:
#   file.managed:
#     - names:
#       - /var/www/banjocms/logs/nginx-access.log
#       - /var/www/banjocms/logs/nginx-error.log
#     - makedirs: True
#     - replace: False
#     - user: {{ pillar['user-name'] }}
#     - group: {{ pillar['user-name'] }}
#     - mode: 777
    
nginx:
  pkg:
    - installed
  service:
    - running
    - name: nginx
    - enable: True
    - reload: True
    - watch:
      - pkg: nginx
      - file: /etc/nginx/nginx.conf
      - file: /etc/nginx/sites-available/default

/etc/nginx/nginx.conf:
  file.line:
    - match: "sendfile on;"
    - content: "sendfile off;"
    - mode: replace
    - watch_in:
      - service: nginx
 
/etc/nginx/sites-available/default:
  file.managed:
    - source: salt://nginx/default
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - watch_in:
      - service: nginx

/etc/nginx/sites-available/banjo.local:
  file.managed:
    - source: salt://nginx/banjo.local
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - watch_in:
      - service: nginx

/etc/nginx/sites-available/ror.local:
  file.managed:
    - source: salt://nginx/ror.local
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - watch_in:
      - service: nginx

/etc/nginx/sites-enabled/banjo.local:
  file.symlink:
    - target: /etc/nginx/sites-available/banjo.local
    - watch_in:
      - service: nginx
      
/etc/nginx/sites-enabled/ror.local:
  file.symlink:
    - target: /etc/nginx/sites-available/ror.local
    - watch_in:
      - service: nginx