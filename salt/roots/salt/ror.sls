ror:
  pkg:
    - installed
    - names:
      - ruby
      - ruby-dev
      - libsqlite3-dev
      #- libgmp3-dev
      - rails

gems:
  gem:
    - installed
    - names:
      - unicorn
      - mysql2

/var/www/{{ pillar['ruby-app'] }}/config/unicorn.rb:
  file.managed:
    - makedirs: True
    - source: salt://ror/unicorn.rb
    - template: jinja
    - mode: 777

shared_ror_dirs:
  file.directory:
    - makedirs: True
    - user: {{ pillar['user-name'] }}
    - group: {{ pillar['user-name'] }}
    - mode: 775
    - names:
      - /home/{{ pillar['user-name'] }}/{{ pillar['ruby-app'] }}/shared/pids
      - /home/{{ pillar['user-name'] }}/{{ pillar['ruby-app'] }}/shared/sockets
      - /home/{{ pillar['user-name'] }}/{{ pillar['ruby-app'] }}/shared/log

/etc/init.d/unicorn_ror:
  file.managed:
    - source: salt://ror/unicorn_initd
    - template: jinja
    - user: root
    - group: root
    - mode: 755
  cmd.wait:
    - name: update-rc.d unicorn_{{ pillar['ruby-app'] }} defaults

unicorn_{{ pillar['ruby-app'] }}:
  service.running:
    - enable: True
    - reload: True