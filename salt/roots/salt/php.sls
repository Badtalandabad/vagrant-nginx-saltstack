#php_ppa:
#  pkgrepo.managed:
#    - ppa: ondrej/php

php:
  pkg:
    - installed
    - names:
      - php7.2
      - php7.2-cli
      - php7.2-curl
      - php7.2-dev
      - php7.2-gmp
      - php7.2-gd
      - php7.2-intl
      - php7.2-json
      - php7.2-mbstring
      - php7.2-mysql
      - php7.2-pgsql
      - php7.2-readline
      - php7.2-soap
      - php7.2-sqlite3
      - php7.2-xml
      - php7.2-zip

php7.2-fpm:
  pkg.installed

install xdebug:
  pecl.installed:
    - name: xdebug
    - require:
      - pkg: php7.2

/etc/php/7.2/fpm/php.ini:
  file.append:
    - text:
      - "[xdebug]"
      - "zend_extension=/usr/lib/php/20170718/xdebug.so"
      - "xdebug.remote_enable=1"
      - "xdebug.remote_connect_back=1"
      - "xdebug.var_display_max_depth = -1"
      - "xdebug.var_display_max_children = -1"
      - "xdebug.var_display_max_data = -1"

/var/run/php7.2-fpm.sock:
  file.managed:
    - user: www-data
    - group: www-data
    - mode: 644
    - replace: false
    - watch_in:
      - service: php7.2-fpm

